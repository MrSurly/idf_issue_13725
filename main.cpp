

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string>
#include <sys/random.h>
#include <xtensa/core-macros.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <assert.h>

#include "esp_log.h"
#include "driver/gpio.h"
#include <sys/types.h>
#include <dirent.h>
#include "esp_spiffs.h"
#include <driver/ledc.h>

const char * TAG = "main";


// returns between [start] and [end - 1]
static uint32_t rand_range(uint32_t start, uint32_t end) {
    uint32_t range = end - start;
    float q = (float)random() / (float)INT32_MAX;
    return range * q + start;
}

static void spiffsInit(void) {
  esp_vfs_spiffs_conf_t conf = {.base_path = "/spiffs",
                                .partition_label = NULL,
                                .max_files = 10,
                                .format_if_mount_failed = false};

  // Use settings defined above to initialize and mount SPIFFS filesystem.
  // Note: esp_vfs_spiffs_register is an all-in-one convenience function.
  esp_err_t ret = esp_vfs_spiffs_register(&conf);

  if (ret != ESP_OK) {
    if (ret == ESP_FAIL) {
      ESP_LOGE(TAG, "Failed to mount or format filesystem");
    } else if (ret == ESP_ERR_NOT_FOUND) {
      ESP_LOGE(TAG, "Failed to find SPIFFS partition");
    } else {
      ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
    }
  } else {
    ESP_LOGE(TAG, "SPIFFS ok");
  }
}


static void spiffs_task(void * arg) {
  uint8_t buf[128];
  FILE * f = fopen("/spiffs/loop2.wav", "r");
  assert(f != NULL);
  while (true) {
    int res = fread(buf, sizeof(buf), 1, f);
    if (res == 0) {
      fseek(f, 0, SEEK_SET);
    }
    vTaskDelay(10 / portTICK_PERIOD_MS);
  }

}

#define LEDC_TIMER                      LEDC_TIMER_0
#define LEDC_MODE                       LEDC_LOW_SPEED_MODE
#define LEDC_OUTPUT_IO                  (5) // Define the output GPIO
#define LEDC_CHANNEL                    LEDC_CHANNEL_0
#define LEDC_DUTY_RES                   LEDC_TIMER_13_BIT // Set duty resolution to 13 bits
#define LEDC_FREQUENCY                  (5000) // Frequency in Hertz. Set frequency at 5 kHz
#define LED_FADE_TIME                   1000

static void led_task (void* arg) {
  // Prepare and then apply the LEDC PWM timer configuration
  ledc_timer_config_t ledc_timer = {
      .speed_mode = LEDC_MODE,
      .duty_resolution = LEDC_DUTY_RES,
      .timer_num = LEDC_TIMER,
      .freq_hz = LEDC_FREQUENCY,  // Set output frequency at 5 kHz
      .clk_cfg = LEDC_AUTO_CLK};
  ESP_ERROR_CHECK(ledc_timer_config(&ledc_timer));

  // Prepare and then apply the LEDC PWM channel configuration
  ledc_channel_config_t ledc_channel = {.gpio_num = GPIO_NUM_8,
                                        .speed_mode = LEDC_MODE,
                                        .channel = LEDC_CHANNEL,
                                        .intr_type = LEDC_INTR_DISABLE,
                                        .timer_sel = LEDC_TIMER,
                                        .duty = 0,  // Set duty to 0%
                                        .hpoint = 0,
                                        .flags = 0};
  ESP_ERROR_CHECK(ledc_channel_config(&ledc_channel));
  ESP_ERROR_CHECK(ledc_fade_func_install(0));

  while (1) {
    uint32_t delay_ms = rand_range(0, 1000);
    vTaskDelay(delay_ms / portTICK_PERIOD_MS);

    uint32_t duty = rand_range(0, 8000);
    ESP_ERROR_CHECK(ledc_set_fade_time_and_start(
        LEDC_MODE, LEDC_CHANNEL, duty, LED_FADE_TIME, LEDC_FADE_NO_WAIT));
  }
}

extern "C" void app_main(void) {
  spiffsInit();

xTaskCreate(spiffs_task, "spiffs", 4096, NULL, 1, NULL);
xTaskCreate(led_task, "led", 4096, NULL, 1, NULL);


}