

.PHONY: FORCE
.ONESHELL:

all: 
	pio run

upload: 
	pio run -t upload

uploadmonitor: 
	pio run -t upload -t monitor

monitor: 
	pio run  -t monitor


uploadfs:
	pio run -t uploadfs

clean:
	pio run -t clean
	rm -Rf $(VERSION_FILE)

